<?php

// Autoload dependencies and helper functions
require "vendor/autoload.php";
require "vendor/illuminate/support/helpers.php";
//

// Register class loader
$load_directories = [
    __DIR__ . "/server/controllers",
    __DIR__ . "/server/models"
];

Illuminate\Support\ClassLoader::register();
Illuminate\Support\ClassLoader::addDirectories($load_directories);
//


// Setup Eloquent
$capsule = new Illuminate\Database\Capsule\Manager;

foreach(require "server/config/database.php" as $key => $value)
{
    $capsule->addConnection($value, $key);
}

$capsule->bootEloquent();
//


// Setup service providers
$app = new Illuminate\Container\Container;
$app['env'] = 'production';

$app->bind('app', $app);
$app->bind('path.base', __DIR__ . "/server");

(new Illuminate\Events\EventServiceProvider($app))->register();
(new Illuminate\Routing\RoutingServiceProvider($app))->register();
(new Illuminate\Filesystem\FilesystemServiceProvider($app))->register();

$app->singleton('config', function() {
    $config = new Illuminate\Config\Repository;
    $config->set('view.compiled', __DIR__ . "/storage/views");
    $config->set('view.paths', [ __DIR__ . "/server/views" ]);
    return $config;
});

(new Illuminate\View\ViewServiceProvider($app))->register();

Illuminate\Support\Facades\Facade::setFacadeApplication($app);
//


// Setup error handler
$whoops = new Whoops\Run;
$whoops->pushHandler(new Whoops\Handler\PrettyPageHandler);
$whoops->register();
//


// Setup Aliases
foreach (require "server/config/alias.php" as $alias => $fully_qualified_name)
{
    class_alias($fully_qualified_name, $alias);
}
//


// Setup router
$route = $app['router'];

$route->get('/client/{file}', function($file) {
    return Illuminate\Support\Facades\File::get(__DIR__ . "/client/$file");
});

require __DIR__ . "/server/config/project_constants.php";
require __DIR__ . "/server/helpers.php";
require __DIR__ . "/server/routes.php";
require __DIR__ . "/server/filters.php";
//


// Setup Request instance
$app->singleton('request', function() use ($app) {
    Illuminate\Http\Request::enableHttpMethodParameterOverride();
    return Illuminate\Http\Request::createFromGlobals();
});
//


// Setup UrlGenerator for application
$app->singleton('url', function() use ($app) {
    $route_collection = $app['router']->getRoutes();
    $request = $app['request'];
    return new Illuminate\Routing\UrlGenerator($route_collection, $request);
});
//


// Add route helper function
if (!function_exists('route'))
{
    function route($name, $parameters = array(), $absolute = true, $route = null)
    {
        $app = Illuminate\Support\Facades\Facade::getFacadeApplication();
        return $app['url']->route($name, $parameters, $absolute, $route);
    };
}
//


// Get request and send response
$request = $app['request'];
$request->method();
$response = $route->dispatch($request);
$response->send();
//