<!DOCTYPE html>
<html>

    <head>

        <title>
            @section('title')
                Index
            @show
        </title>

        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/css/materialize.min.css">
        <link rel="stylesheet" href="/{{ PROJECT_NAME }}/client/styles/template.css">
        <link rel="icon" type="image/ico" href="/{{ PROJECT_NAME }}/client/images/favicon.ico?v=2">

        @section('styles')

        @show

    </head>

    <body>

        <nav id="side-nav" class="indigo darken-4">
            <ul id="side-nav-menu">
                <li class="side-nav-menu-item">
                    <a href="/{{ PROJECT_NAME }}">
                        Home
                    </a>
                </li>
                <li class="side-nav-menu-item">
                    <a href="/{{ PROJECT_NAME }}/about">
                        About
                    </a>
                </li>
                <li class="side-nav-menu-item">
                    <a href="/{{ PROJECT_NAME }}/resume">
                        Resume
                    </a>
                </li>
                <li class="side-nav-menu-item">
                    <a href="/{{ PROJECT_NAME }}/portfolio">
                        Portfolio
                    </a>
                </li>
                <li class="side-nav-menu-item">
                    <a href="/{{ PROJECT_NAME }}/contact">
                        Contact
                    </a>
                </li>
                <li class="side-nav-menu-item">
                    <a href="/{{ PROJECT_NAME }}/guest_book">
                        Guest Book
                    </a>
                </li>
            </ul>
        </nav>

        <main id="content">
            @section('content')

            @show
        </main>

        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/parallax/2.1.3/jquery.parallax.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
        <script src="/{{ PROJECT_NAME }}/client/scripts/template.js"></script>

        @section('scripts')

        @show

    </body>

</html>