<?php
/**
 * Created by PhpStorm.
 * User: Nephi
 * Date: 7/3/2016
 * Time: 10:46 PM
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Model extends Eloquent
{
    protected $connection = 'mysql';
    protected $table = 'test';
}