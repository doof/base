<?php

return [
    'View' => 'Illuminate\\Support\\Facades\\View',
    'Route' => 'Illuminate\\Support\\Facades\\Route',
    'Redirect' => 'Illuminate\\Support\\Facades\\Redirect',
    'Eloquent' => 'Illuminate\\Database\\Eloquent\\Model',
    'Controller' => 'Illuminate\\Routing\\Controller',
    'FPDF' => 'fpdf\\FPDF'
];