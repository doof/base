<?php

/**
 * I pledge that this submission is solely my work, and that I have neither 
 * given to nor received help from anyone other than the instructor or TAs.
 */

return [
    
    /* 
     * These are used for all of your separate database connections. If you
     * have multiple connections to the same type of database, just make sure
     * to give it a different key, e.g.
     * 
     * 'mysql2' => [ ... ]
     */
    
    'sqlite' => [
        'driver'   => 'sqlite',
        'database' => __DIR__.'/../database/production.sqlite',
        'prefix'   => '',
    ],

    'mysql' => [
        'driver'    => 'mysql',
        'host'      => 'localhost',
        'database'  => 'base',
        'username'  => 'root',
        'password'  => '',
        'charset'   => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix'    => '',
    ],

    'pgsql' => [
        'driver'   => 'pgsql',
        'host'     => 'localhost',
        'database' => 'forge',
        'username' => 'forge',
        'password' => '',
        'charset'  => 'utf8',
        'prefix'   => '',
        'schema'   => 'public',
    ],

    'sqlsrv' => [
        'driver'   => 'sqlsrv',
        'host'     => 'localhost',
        'database' => 'database',
        'username' => 'root',
        'password' => '',
        'prefix'   => '',
    ],
    
    'oracle' => [
        'driver'   => 'oracle',
        'host'     => 'db1.aux.cwu.edu',
        'port'     => '1585',
        'database' => 'pktst',
        'username' => 'flexadmin',
        'password' => 'ame3m',
        'charset'  => 'AL32UTF8',
        'prefix'   => '',
    ]
    
];

