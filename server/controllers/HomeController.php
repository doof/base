<?php

namespace App\Controllers;

use View;
use App\Models\Post;

class HomeController
{

    public function index()
    {
        $posts = Post::all();

        return View::make('home.index', ['posts' => $posts]);
    }

}