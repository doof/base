<?php

if (!function_exists('get_angular_params'))
{
    function get_angular_params()
    {
        return json_decode(file_get_contents('php://input', true));
    }
}